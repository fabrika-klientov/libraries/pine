export interface DiscountModel extends DiscountValues {
    id: number;
    name: string;
    title: string;
    description: string;
    type: 'widget';
    typeDiscount: 'percent' | 'diff';
    oncePer?: boolean;
    promo?: boolean;
    promoCode?: string | number;
    owned?: boolean;
    start?: string;
    expired?: string;
    availableCount?: number;
    count?: number;
    successCount?: number;
    revoked?: boolean;
    additional?: DiscountAdditional;
}

export interface DiscountAdditional {
    currencies?: DiscountCurrency[];
}

export interface DiscountCurrency extends DiscountValues {
    currency: string;
}

export interface DiscountValues {
    discount: string | number;
    maxDiscount?: string | number;
    round: string | number; // (-10|-5|-1|0|+1|+5|+10) 0 - do not round
}
