export interface HttpClient {
    get(link: string, query?: any): Promise<any> | any;
    post(link: string, data: any): Promise<any> | any;
}
