import React, {Component} from 'react';
import {Modal} from './pine-modal';

export class Overload extends Component<any, any> {

    protected detailsRef: HTMLElement;

    public onCLose() {
        this.detailsRef.removeAttribute('open');
    }

    public setRef(el: HTMLElement | any) {
        this.detailsRef = el;
    }

    render() {
        return (
            <details ref={(el) => this.setRef(el)}>
                <summary>
                    <div className="button-input button-input_blue">
                        <span className="button-input-inner">
                            <span className="button-input-inner__text">Оплатить виджет</span>
                        </span>
                    </div>
                    <div className="details-modal-overlay"/>
                </summary>
                <Modal
                    onClose={() => this.onCLose()}
                    pricePeriods={this.props.pricePeriods}
                    discountList={this.props.discountList}
                />
            </details>
        );
    }
}
