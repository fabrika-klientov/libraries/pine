import {Component} from 'react';
import React from 'react';

interface ModalProps {
    link: string;
    onClose(): void;
}

let context: PayModal = null;
const clickBodyListener = event => {
    let target = event.target;
    if (context) {
        if (target === context.modalRef || context.modalRef.contains(target)) {
            return;
        }
        context.close();
    }
};

export class PayModal extends Component<ModalProps, any> {

    public modalRef: HTMLElement;

    constructor(props) {
        super(props);
        context = this;

        this.state = {};
    }

    componentDidMount() {
        this.removeListeners();
        this.addListeners();
    }

    componentWillUnmount() {
        this.removeListeners();
    }

    protected addListeners() {
        document.body.addEventListener('click', clickBodyListener, false);
    }

    protected removeListeners() {
        document.body.removeEventListener('click', clickBodyListener, false);
    }

    public setRef(el: HTMLElement | any) {
        this.modalRef = el;
    }

    public close() {
        this.props.onClose();
    }

    render() {
        return (
            <div className="p-p-modal-overload">
                <div ref={(el) => this.setRef(el)} className="p-p-modal">
                    <iframe src={this.props.link} frameBorder="0"></iframe>
                </div>
            </div>
        );
    }
}
