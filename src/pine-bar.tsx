import React, {Component} from 'react';

interface Alert {
    type: string;
    title: string;
    description: string;
}

interface PineBarProps {
    alert: Alert;
}

export class PineBar extends Component<PineBarProps, any> {

    public get title(): string {
        return this.props.alert.title || '';
    }

    public get desc(): string {
        return this.props.alert.description || '';
    }

    public get type(): string {
        return this.props.alert.type || '';
    }

    render() {
        return (
            <div className={`pine-alert pine-alert-${this.type}`}>
                <div className="pine-alert__content">
                    <span className="pine-alert__title">{this.title}</span>
                    <p className="pine-alert__description" dangerouslySetInnerHTML={{ __html: this.desc }} />
                </div>
            </div>
        );
    }
}
