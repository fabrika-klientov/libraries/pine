import './styles.scss';

import ReactDOM from 'react-dom';
import React from 'react';
import {Overload} from './pine-overload';
import {PricePeriodSettings} from './price-period.model';
import {HttpClient} from './http-client';
import {PineBar} from './pine-bar';
import {CONFIG} from './config';
import {RequestHelper} from './request.helper';
import {StatusData} from './status-data.model';
import {DiscountModel} from './discount.model';

export interface PineAccountData {
    domain: string;
    account: string;
    code: string;
    key: string;
    widgetParams: any;
    pricePeriods: PricePeriodSettings[];
    currentPeriod: any;
    discountList: DiscountModel[];
}


export class Pine {

    public static accountData: PineAccountData;
    private static http: HttpClient;

    constructor(protected element: HTMLElement | any, httpClient: HttpClient) {
        if (typeof this.element === 'string') {
            this.element = document.querySelector(this.element) as HTMLElement;
        }
        Pine.http = httpClient;
    }

    public init(account: PineAccountData) {
        Pine.accountData = account;
        if (!this.element) {
            return;
        }

        if (account.widgetParams?.active !== 'Y') {
            return;
        }

        ReactDOM.render(
            React.createElement(Overload, {pricePeriods: account.pricePeriods, discountList: account.discountList || []}),
            this.element,
        );
    }

    public initBar(account: PineAccountData) {
        if (!this.element) {
            return;
        }

        RequestHelper.work(Pine.http.get(CONFIG.server + CONFIG.statusLink + account.key), (result: StatusData) => {
            RequestHelper.work(
                Pine.http.get(CONFIG.serverText + (CONFIG.statusText
                    .replace('{code}', result.code)
                    .replace('{lang}', 'ru'))
                ),
                (status: any) => {
                    let description = status || '';
                    for (let key in result.additional) {
                        description = description.replace(key, result.additional[key]);
                    }

                    let alert = {
                        type: result.type,
                        title: ['success', 'info'].includes(result.type)
                            ? 'Виджет активен'
                            : (result.type === 'warning' ? 'Предупреждение' : 'Внимание'),
                        description,
                    }

                    ReactDOM.render(React.createElement(PineBar, {alert}), this.element);
                },
            )
        });
    }

    public withStyles(): this {
        let parentBox = this.element.classList.contains('widget_settings_block__controls')
            ? this.element : this.element.closest('.widget_settings_block__controls');

        if (parentBox) {
            parentBox.classList.add('flex-pay-btn-box');
        }

        return this;
    }

    public static get httpClient(): HttpClient {
        return Pine.http;
    }
}
