export interface StatusData {
    type: string;
    code: string;
    additional?: {
        period_start?: string;
        period_end?: string;
    };
}
