import ReactDOM from 'react-dom';
import {ChangeEvent, Component} from 'react';
import React from 'react';
import {PricePeriodCurrency, PricePeriodSettings} from './price-period.model';
import {Pine} from './index';
import {$APP, CONFIG} from './config';
import {RequestHelper} from './request.helper';
import {AmoManager, AmoUser} from './amo-user.model';
import {PayModal} from './pay-modal';
import {DiscountModel, DiscountValues} from './discount.model';

interface ModalProps {
    pricePeriods: PricePeriodSettings[];
    discountList: DiscountModel[];
    onClose(): void;
}

interface ModalState {
    name: string;
    phone: string;
    email: string;
    period: PricePeriodSettings | null;
    currency: PricePeriodCurrency | null;
    discount: DiscountModel | null;
    promoCode: string | null;
    loading?: boolean;
    loadingPromo?: boolean;
    showPromo: boolean;
    blockedPromo?: boolean;
    invalidPromo?: boolean;
    finished?: boolean;
}

interface FilterPeriodState {
    min: PricePeriodSettings | null;
    max: PricePeriodSettings | null;
}

export class Modal extends Component<ModalProps, ModalState> {

    private currenciesOrderList: string[] = ['USD', 'UAH', 'RUB'];

    constructor(props) {
        super(props);

        let user = this.getUser();

        this.state = {
            name: user?.name || '',
            phone: user?.personal_mobile || '',
            email: user?.login || '',
            period: null,
            currency: null,
            discount: null,
            promoCode: null,
            showPromo: false,
        };
    }

    componentDidMount() {
        this.setState({
            ...this.state,
            period: this.firstPeriod,
            currency: this.getPayCurrency(this.selectedPeriod),
            discount: this.props.discountList?.length
                ? (this.props.discountList.find(item => item.owned) || this.props.discountList[0]) : null,
        });
    }

    public get availablePeriods(): PricePeriodSettings[] {
        let usersCount = this.managers().length;

        let hp = (statement: FilterPeriodState, item: PricePeriodSettings): void => {
            if (!item.users) {
                statement.max = item;
                return;
            }

            if (item.users < usersCount) {
                return;
            }

            if (!statement.min || statement.min.users > item.users) {
                statement.min = item;
            }
        };

        return this.props.pricePeriods
            .reduce((result: FilterPeriodState[], item) => {
                let existStatement = result.find(
                    ({min, max}) => (min?.period.id === item.period.id || max?.period.id === item.period.id)
                );
                let newStatement = {max: null, min: null};

                hp(existStatement || newStatement, item);
                if (!existStatement) {
                    result.push(newStatement);
                }

                return result;
            }, [])
            .map(({min, max}) => (min || max))
            .filter(item => item);
    }

    public get disableBtn(): boolean {
        return this.state.loading;
    }

    public get disablePromoBtn(): boolean {
        return this.state.loadingPromo || this.state.blockedPromo || !(this.state.promoCode && this.state.promoCode.length);
    }

    public get isSinglePeriod(): boolean {
        return this.availablePeriods.length === 1;
    }

    public isSingleCurrencyPeriod(period: PricePeriodSettings): boolean {
        return period.currency.length === 1;
    }

    public get firstPeriod(): PricePeriodSettings {
        return this.availablePeriods[0];
    }

    public getPayCurrency(model: PricePeriodSettings): PricePeriodCurrency {
        return this.currenciesOrderList
            .reduce(
                (result: PricePeriodCurrency, item) => (result || model.currency.find(one => one.currency.toUpperCase() === item)),
                null,
            ) || model.currency[0];
    }

    public getNumberPeriod(model: PricePeriodSettings): number {
        return +model.period.value.replace(/[^\d]/g, '');
    }

    public getEnumPeriod(model: PricePeriodSettings): string {
        let last = model.period.value.split(' ').pop()!;
        let count = this.getNumberPeriod(model);

        switch (last.toUpperCase()) {
            case 'MONTH':
            case 'MONTHS':
                return count === 1 ? 'месяц' : (count > 4 ? 'месяцев' : 'месяца');
            case 'YEAR':
            case 'YEARS':
                return count === 1 ? 'год' : (count > 4 ? 'лет' : 'года');
            case 'DAY':
            case 'DAYS':
                return count === 1 ? 'день' : (count > 4 ? 'дней' : 'дня');
        }

        return '';
    }

    public get isDiscount(): boolean {
        return !!this.state.discount;
    }

    public get isPromo(): boolean {
        return this.isDiscount && this.state.discount.promo;
    }

    public get isNoPromo(): boolean {
        return this.isDiscount && !this.isPromo;
    }

    public get totalPrice(): number | string {
        let current = this.selectedCurrencyPeriod;
        let discount = this.state.discount;

        if (discount) {
            let discountData: DiscountValues = discount.additional?.currencies?.find(
                item => item.currency.toUpperCase() === current.currency.toUpperCase()
            ) || discount;

            let discountValue: number = 0;
            switch (discount.typeDiscount) {
                case 'percent':
                    discountValue = ((+current.value) * (+discountData.discount)) / 100;
                    break;
                case 'diff':
                    discountValue = +discountData.discount;
                    break;
            }

            if (discountData.maxDiscount && discountValue > +discountData.maxDiscount) {
                discountValue = +discountData.maxDiscount;
            }

            let totalValue = +current.value - discountValue;

            if (discountData.round) {
                switch (discountData.round) {
                    case '-10':
                        totalValue = Math.floor(totalValue / 10) * 10;
                        break;
                    case '-5':
                        totalValue = 5 * Math.floor(totalValue / 5);
                        break;
                    case '-1':
                        totalValue = Math.floor(totalValue);
                        break;
                    case '+1':
                        totalValue = Math.ceil(totalValue);
                        break;
                    case '+5':
                        totalValue = 5 * Math.ceil(totalValue / 5);
                        break;
                    case '+10':
                        totalValue = Math.ceil(totalValue / 10) * 10;
                        break;
                    case '0':
                }
            }

            return totalValue;
        }

        return current.value;
    }

    public close() {
        this.props.onClose();
    }

    public setFormState(attr: string, ev: ChangeEvent<HTMLInputElement>) {
        this.setState({...this.state, [attr]: ev.target.value});
    }

    public setDiscount(discount: DiscountModel) {
        this.setState({...this.state, discount});
    }

    public changePromoVisible() {
        this.setState({...this.state, showPromo: !this.state.showPromo});
    }

    public setPeriod(period: PricePeriodSettings) {
        this.setState({...this.state, period, currency: this.getPayCurrency(period)});
    }

    public setCurrencyPeriod(currency: PricePeriodCurrency) {
        this.setState({...this.state, currency});
    }

    public get selectedPeriod(): PricePeriodSettings {
        return this.state.period || this.firstPeriod;
    }

    public get selectedCurrencyPeriod(): PricePeriodCurrency {
        return this.state.currency || this.getPayCurrency(this.selectedPeriod);
    }

    public checkPromo() {
        this.setState({...this.state, loadingPromo: true});

        RequestHelper.work(
            Pine.httpClient.post(CONFIG.checkPromoLink, {accountData: Pine.accountData, promo: this.state.promoCode}),
            (data) => {
                if (data?.status) {
                    if (data.data.length) {
                        this.setState({...this.state, loadingPromo: false, blockedPromo: true});
                        this.setDiscount(data.data.find(item => item.owned) || data.data[0]);
                    } else {
                        this.setState({...this.state, promoCode: null, loadingPromo: false, invalidPromo: true});
                        setTimeout(() => this.setState({...this.state, invalidPromo: false}), 5000);
                    }
                }
            }
        );
    }

    public send() {
        this.setState({...this.state, loading: true});

        RequestHelper.work(
            Pine.httpClient.post(CONFIG.payLink, {accountData: Pine.accountData, payData: this.state}),
            (data) => {
                if (data?.status) {
                    this.setState({...this.state, loading: false, finished: true});
                    this.close();
                    if (CONFIG.payInModal) {
                        this.payIframe(data.data.url);
                    } else {
                        window.open(data.data.url, '_blank');
                    }
                }
            }
        );
    }

    protected payIframe(link: string) {
        let payBox = document.createElement('div');
        payBox.id = CONFIG.payIframeId;
        document.body.append(payBox);
        ReactDOM.render(React.createElement(PayModal, {link, onClose: () => this.closePayIframe()}), payBox);
    }

    protected closePayIframe() {
        let payBox = document.getElementById(CONFIG.payIframeId);
        if (payBox) {
            payBox.remove();
        }
    }

    protected getUser(): AmoUser {
        return $APP.constant('user');
    }

    public managers(isActive: boolean = true): AmoManager[] {
        let result: AmoManager[] = [];
        let managers = $APP.constant('managers');
        for (let id in managers) {
            if (!managers.hasOwnProperty(id)) {
                continue;
            }

            result.push(managers[id]);
        }

        if (isActive) {
            return result.filter(item => item.active);
        }

        return result;
    }

    public get selectPeriods(): JSX.Element {
        let selectItems = this.availablePeriods.map((item, index) =>
            <li
                data-value={index}
                className={`control--select--list--item ${index === 0 ? 'control--select--list--item-selected' : ''}`}
                onClick={() => this.setPeriod(item)}
                key={index}>
                <span className="control--select--list--item-inner" title={`${this.getNumberPeriod(item)} ${this.getEnumPeriod(item)}`}>
                    {this.getNumberPeriod(item)} {this.getEnumPeriod(item)}
                </span>
            </li>
        );

        return <div className="control--select select-pay-period">
            <ul className="custom-scroll control--select--list">
                {selectItems}
            </ul>
            <button className="control--select--button" type="button" data-value="0">
                <span className="control--select--button-inner">{this.getNumberPeriod(this.selectedPeriod)} {this.getEnumPeriod(this.selectedPeriod)}</span>
            </button>
            <input type="hidden" className="control--select--input" name="payPeriod" value="0" data-prev-value="0" data-change-on-init="true" />
        </div>;
    }

    public get selectCurrenciesPeriod(): JSX.Element {
        let selectedCP = this.selectedCurrencyPeriod;

        let selectItems = this.selectedPeriod.currency.map((item, index) =>
            <li
                data-value={index}
                className={`control--select--list--item ${item === selectedCP ? 'control--select--list--item-selected' : ''}`}
                onClick={() => this.setCurrencyPeriod(item)}
                key={index}>
                <span className="control--select--list--item-inner" title={item.currency}>{item.currency}</span>
            </li>
        );

        let selectedIndex = this.selectedPeriod.currency
            .reduce((result, item, index) => item === selectedCP ? index : result, 0);

        return <div className="control--select select-pay-currency-period">
            <ul className="custom-scroll control--select--list">
                {selectItems}
            </ul>
            <button
                className="control--select--button"
                type="button"
                data-value={selectedIndex}>
                <span className="control--select--button-inner">{selectedCP.currency}</span>
            </button>
            <input type="hidden" className="control--select--input" name="payCurrencyPeriod" value={selectedIndex} data-prev-value="0" data-change-on-init="true" />
        </div>;
    }

    render() {
        let currencyPeriod = this.selectedCurrencyPeriod;

        let currencyPrice = <span key={currencyPeriod.currency}><span>{this.totalPrice} {currencyPeriod.curVal}</span></span>;
        let currencyRedPrice = <span key={currencyPeriod.currency}><span>{currencyPeriod.value} {currencyPeriod.curVal}</span></span>;


        return (
            <div className="details-modal">
                <div className="details-modal-close" onClick={() => this.close()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                        <path
                            fillRule="evenodd"
                            clipRule="evenodd"
                            d="M13.7071 1.70711C14.0976 1.31658 14.0976 0.683417 13.7071 0.292893C13.3166 -0.0976311 12.6834 -0.0976311 12.2929 0.292893L7 5.58579L1.70711 0.292893C1.31658 -0.0976311 0.683417 -0.0976311 0.292893 0.292893C-0.0976311 0.683417 -0.0976311 1.31658 0.292893 1.70711L5.58579 7L0.292893 12.2929C-0.0976311 12.6834 -0.0976311 13.3166 0.292893 13.7071C0.683417 14.0976 1.31658 14.0976 1.70711 13.7071L7 8.41421L12.2929 13.7071C12.6834 14.0976 13.3166 14.0976 13.7071 13.7071C14.0976 13.3166 14.0976 12.6834 13.7071 12.2929L8.41421 7L13.7071 1.70711Z"
                            fill="black"/>
                    </svg>
                </div>

                <div className="details-modal-title">
                    <h1>Оплатить виджет онлайн</h1>
                </div>

                <div className="details-modal-content">
                    <div className="main-content">

                        <div className="line-select-data">
                            <div>Валюта для оплаты:</div>
                            <div className="right-line">
                                {
                                    this.isSingleCurrencyPeriod(this.selectedPeriod)
                                        ? <span>{this.selectedCurrencyPeriod.currency}</span>
                                        : this.selectCurrenciesPeriod
                                }
                            </div>
                        </div>

                        <div className="line-select-data">
                            <div>Период:</div>
                            <div className="right-line">
                                {
                                    this.isSinglePeriod
                                        ? <span>{this.getNumberPeriod(this.selectedPeriod)} {this.getEnumPeriod(this.selectedPeriod)}</span>
                                        : this.selectPeriods
                                }
                            </div>
                        </div>

                        <div className={`prices-data ${this.isNoPromo ? 'with-discount' : ''}`}>
                            { this.isNoPromo && <p className="text-discount">{this.state.discount.title}</p> }
                            {
                                this.isNoPromo && this.state.discount.description?.length &&
                                <p className="desc-discount">{this.state.discount.description}</p>
                            }
                            <div className="text-price">{currencyPrice}</div>
                            { this.isDiscount && <div className="text-price-red">{currencyRedPrice}</div> }
                            {
                                this.selectedPeriod.users && <p className="text-users">
                                    <span>По тарифу: «До {this.selectedPeriod.users} пользователей»<sup>*</sup></span>
                                    <div className="users-notice-info">
                                        <sup>*</sup>
                                        Тариф определяется автоматически, исходя из количества пользователей в аккаунте Kommo (amoCRM).
                                    </div>
                                </p>
                            }
                        </div>

                        <div className="pine-promo-box">
                            {
                                !this.state.blockedPromo &&
                                <div className="pine-promo-act">
                                    <span onClick={() => this.changePromoVisible()}>У меня есть промокод</span>
                                </div>
                            }

                            {
                                this.state.showPromo &&
                                <div className="pine-promo-code">
                                    <input
                                        name="pine-promo-code"
                                        value={this.state.promoCode}
                                        onChange={(e) => this.setFormState('promoCode', e)}
                                        className="text-input"
                                        type="text"
                                        placeholder="Ваш промокод"
                                        disabled={this.state.blockedPromo}
                                    />
                                    <button
                                        type="button"
                                        className={`to-promo-btn button-input ${this.disablePromoBtn ? 'button-input-disabled' : ''}`}
                                        onClick={() => this.disablePromoBtn ? null : this.checkPromo()}>
                                    <span className="button-input-inner">
                                        { this.state.loadingPromo && <span className="spinner-icon" /> }
                                        <span className="button-input-inner__text">Применить</span>
                                    </span>
                                    </button>
                                </div>
                            }

                            <div className="info-promo">
                                {
                                    this.state.blockedPromo &&
                                    <span className="success-promo">Промокод успешно применен!</span>
                                }
                                {
                                    this.state.invalidPromo &&
                                    <span className="error-promo">Промокод не найден или срок действия истек</span>
                                }
                            </div>
                        </div>

                        <button
                            type="button"
                            className={`to-pay-btn button-input button-input_blue ${this.disableBtn ? 'button-input-disabled' : ''}`}
                            onClick={() => this.disableBtn ? null : this.send()}>
                            <span className="button-input-inner">
                                { this.state.loading && <span className="spinner-icon" /> }
                                <span className="button-input-inner__text">Оплатить</span>
                            </span>
                        </button>

                        <div className="agg-privacy">
                            <span>
                                Если вы хотите произвести оплату другим способом, <a href={CONFIG.contactPage} target="_blank">свяжитесь с нами</a>.
                            </span>
                        </div>
                    </div>

                    <div className="footer-content">
                        <div className="flex-contacts">
                            <div className="contacts-phones">
                                <p>
                                    <a href={'tel:'+CONFIG.phone1.replace(/[^+\d]/, '')}>{CONFIG.phone1}</a>
                                </p>
                                <p>
                                    <a href={'tel:'+CONFIG.phone2.replace(/[^+\d]/, '')}>{CONFIG.phone2}</a>
                                </p>
                            </div>
                            <div className="contacts-site">
                                <a href={CONFIG.site} target="_blank">fabrika-klientov.ua</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
