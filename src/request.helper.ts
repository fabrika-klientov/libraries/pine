export class RequestHelper {
    public static work(request: Promise<any> | any, callback): void {
        if (request.then) {
            request.then(callback);
        } else if (request.subscribe) {
            request.subscribe(callback);
        }
    }
}
