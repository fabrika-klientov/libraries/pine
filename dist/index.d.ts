import './styles.scss';
import { PricePeriodSettings } from './price-period.model';
import { HttpClient } from './http-client';
import { DiscountModel } from './discount.model';
export interface PineAccountData {
    domain: string;
    account: string;
    code: string;
    key: string;
    widgetParams: any;
    pricePeriods: PricePeriodSettings[];
    currentPeriod: any;
    discountList: DiscountModel[];
}
export declare class Pine {
    protected element: HTMLElement | any;
    static accountData: PineAccountData;
    private static http;
    constructor(element: HTMLElement | any, httpClient: HttpClient);
    init(account: PineAccountData): void;
    initBar(account: PineAccountData): void;
    withStyles(): this;
    static get httpClient(): HttpClient;
}
