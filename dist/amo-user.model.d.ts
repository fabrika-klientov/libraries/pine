export interface AmoUser {
    amojo_id: string;
    api_key: string;
    group_mates_ids: any[];
    id: number;
    login: string;
    name: string;
    personal_mobile: string;
    settings: any;
    uuid: string;
}
export interface AmoManager {
    active: boolean;
    amo_profile_id: string;
    amojo_id: string;
    avatar: string;
    free_user: 'N' | 'Y';
    group: string;
    id: string;
    is_admin: 'N' | 'Y';
    login: string;
    option: string;
    status: string;
    title: string;
}
