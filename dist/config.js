"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.$APP = exports.CONFIG = void 0;
exports.CONFIG = {
    payLink: "".concat(process.env.VUE_APP_INT_URL).concat(process.env.VUE_APP_PEY_LINK),
    checkPromoLink: "".concat(process.env.VUE_APP_INT_URL).concat(process.env.VUE_APP_CHECK_PROMO_LINK),
    server: process.env.VUE_APP_URL || 'https://api.bpmcenter.pro',
    serverText: process.env.VUE_APP_JSTORAGE_URI || 'https://text.bpmcenter.pro',
    statusLink: '/api/v0/status/',
    statusText: '/texts/shared.messages.{code}/{lang}',
    privacy1: 'https://fabrika-klientov.ua/privacy-policy',
    privacy2: '#',
    contactPage: 'https://fabrika-klientov.ua/kontakty',
    phone1: '+38(044) 499-95-59',
    phone2: '+38(067) 462-74-05',
    site: 'https://fabrika-klientov.ua',
    payInModal: false,
    payIframeId: 'fkPinePayBox',
};
exports.$APP = APP || AMOCRM;
//# sourceMappingURL=config.js.map