import { Component } from 'react';
interface Alert {
    type: string;
    title: string;
    description: string;
}
interface PineBarProps {
    alert: Alert;
}
export declare class PineBar extends Component<PineBarProps, any> {
    get title(): string;
    get desc(): string;
    get type(): string;
    render(): JSX.Element;
}
export {};
