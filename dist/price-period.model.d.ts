export interface PricePeriodSettings {
    period: PeriodCost;
    currency: PricePeriodCurrency[];
    users?: number;
}
export interface PricePeriodCurrency {
    currency: string;
    curVal: string;
    value: string;
}
export interface PeriodCost {
    id?: number;
    name: string;
    trial: boolean;
    value: string;
    interval: number;
}
