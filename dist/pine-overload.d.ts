import { Component } from 'react';
export declare class Overload extends Component<any, any> {
    protected detailsRef: HTMLElement;
    onCLose(): void;
    setRef(el: HTMLElement | any): void;
    render(): JSX.Element;
}
