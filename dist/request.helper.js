"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestHelper = void 0;
var RequestHelper = (function () {
    function RequestHelper() {
    }
    RequestHelper.work = function (request, callback) {
        if (request.then) {
            request.then(callback);
        }
        else if (request.subscribe) {
            request.subscribe(callback);
        }
    };
    return RequestHelper;
}());
exports.RequestHelper = RequestHelper;
//# sourceMappingURL=request.helper.js.map