"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Modal = void 0;
var react_dom_1 = __importDefault(require("react-dom"));
var react_1 = require("react");
var react_2 = __importDefault(require("react"));
var index_1 = require("./index");
var config_1 = require("./config");
var request_helper_1 = require("./request.helper");
var pay_modal_1 = require("./pay-modal");
var Modal = (function (_super) {
    __extends(Modal, _super);
    function Modal(props) {
        var _this = _super.call(this, props) || this;
        _this.currenciesOrderList = ['USD', 'UAH', 'RUB'];
        var user = _this.getUser();
        _this.state = {
            name: (user === null || user === void 0 ? void 0 : user.name) || '',
            phone: (user === null || user === void 0 ? void 0 : user.personal_mobile) || '',
            email: (user === null || user === void 0 ? void 0 : user.login) || '',
            period: null,
            currency: null,
            discount: null,
            promoCode: null,
            showPromo: false,
        };
        return _this;
    }
    Modal.prototype.componentDidMount = function () {
        var _a;
        this.setState(__assign(__assign({}, this.state), { period: this.firstPeriod, currency: this.getPayCurrency(this.selectedPeriod), discount: ((_a = this.props.discountList) === null || _a === void 0 ? void 0 : _a.length)
                ? (this.props.discountList.find(function (item) { return item.owned; }) || this.props.discountList[0]) : null }));
    };
    Object.defineProperty(Modal.prototype, "availablePeriods", {
        get: function () {
            var usersCount = this.managers().length;
            var hp = function (statement, item) {
                if (!item.users) {
                    statement.max = item;
                    return;
                }
                if (item.users < usersCount) {
                    return;
                }
                if (!statement.min || statement.min.users > item.users) {
                    statement.min = item;
                }
            };
            return this.props.pricePeriods
                .reduce(function (result, item) {
                var existStatement = result.find(function (_a) {
                    var min = _a.min, max = _a.max;
                    return ((min === null || min === void 0 ? void 0 : min.period.id) === item.period.id || (max === null || max === void 0 ? void 0 : max.period.id) === item.period.id);
                });
                var newStatement = { max: null, min: null };
                hp(existStatement || newStatement, item);
                if (!existStatement) {
                    result.push(newStatement);
                }
                return result;
            }, [])
                .map(function (_a) {
                var min = _a.min, max = _a.max;
                return (min || max);
            })
                .filter(function (item) { return item; });
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "disableBtn", {
        get: function () {
            return this.state.loading;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "disablePromoBtn", {
        get: function () {
            return this.state.loadingPromo || this.state.blockedPromo || !(this.state.promoCode && this.state.promoCode.length);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "isSinglePeriod", {
        get: function () {
            return this.availablePeriods.length === 1;
        },
        enumerable: false,
        configurable: true
    });
    Modal.prototype.isSingleCurrencyPeriod = function (period) {
        return period.currency.length === 1;
    };
    Object.defineProperty(Modal.prototype, "firstPeriod", {
        get: function () {
            return this.availablePeriods[0];
        },
        enumerable: false,
        configurable: true
    });
    Modal.prototype.getPayCurrency = function (model) {
        return this.currenciesOrderList
            .reduce(function (result, item) { return (result || model.currency.find(function (one) { return one.currency.toUpperCase() === item; })); }, null) || model.currency[0];
    };
    Modal.prototype.getNumberPeriod = function (model) {
        return +model.period.value.replace(/[^\d]/g, '');
    };
    Modal.prototype.getEnumPeriod = function (model) {
        var last = model.period.value.split(' ').pop();
        var count = this.getNumberPeriod(model);
        switch (last.toUpperCase()) {
            case 'MONTH':
            case 'MONTHS':
                return count === 1 ? 'месяц' : (count > 4 ? 'месяцев' : 'месяца');
            case 'YEAR':
            case 'YEARS':
                return count === 1 ? 'год' : (count > 4 ? 'лет' : 'года');
            case 'DAY':
            case 'DAYS':
                return count === 1 ? 'день' : (count > 4 ? 'дней' : 'дня');
        }
        return '';
    };
    Object.defineProperty(Modal.prototype, "isDiscount", {
        get: function () {
            return !!this.state.discount;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "isPromo", {
        get: function () {
            return this.isDiscount && this.state.discount.promo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "isNoPromo", {
        get: function () {
            return this.isDiscount && !this.isPromo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "totalPrice", {
        get: function () {
            var _a, _b;
            var current = this.selectedCurrencyPeriod;
            var discount = this.state.discount;
            if (discount) {
                var discountData = ((_b = (_a = discount.additional) === null || _a === void 0 ? void 0 : _a.currencies) === null || _b === void 0 ? void 0 : _b.find(function (item) { return item.currency.toUpperCase() === current.currency.toUpperCase(); })) || discount;
                var discountValue = 0;
                switch (discount.typeDiscount) {
                    case 'percent':
                        discountValue = ((+current.value) * (+discountData.discount)) / 100;
                        break;
                    case 'diff':
                        discountValue = +discountData.discount;
                        break;
                }
                if (discountData.maxDiscount && discountValue > +discountData.maxDiscount) {
                    discountValue = +discountData.maxDiscount;
                }
                var totalValue = +current.value - discountValue;
                if (discountData.round) {
                    switch (discountData.round) {
                        case '-10':
                            totalValue = Math.floor(totalValue / 10) * 10;
                            break;
                        case '-5':
                            totalValue = 5 * Math.floor(totalValue / 5);
                            break;
                        case '-1':
                            totalValue = Math.floor(totalValue);
                            break;
                        case '+1':
                            totalValue = Math.ceil(totalValue);
                            break;
                        case '+5':
                            totalValue = 5 * Math.ceil(totalValue / 5);
                            break;
                        case '+10':
                            totalValue = Math.ceil(totalValue / 10) * 10;
                            break;
                        case '0':
                    }
                }
                return totalValue;
            }
            return current.value;
        },
        enumerable: false,
        configurable: true
    });
    Modal.prototype.close = function () {
        this.props.onClose();
    };
    Modal.prototype.setFormState = function (attr, ev) {
        var _a;
        this.setState(__assign(__assign({}, this.state), (_a = {}, _a[attr] = ev.target.value, _a)));
    };
    Modal.prototype.setDiscount = function (discount) {
        this.setState(__assign(__assign({}, this.state), { discount: discount }));
    };
    Modal.prototype.changePromoVisible = function () {
        this.setState(__assign(__assign({}, this.state), { showPromo: !this.state.showPromo }));
    };
    Modal.prototype.setPeriod = function (period) {
        this.setState(__assign(__assign({}, this.state), { period: period, currency: this.getPayCurrency(period) }));
    };
    Modal.prototype.setCurrencyPeriod = function (currency) {
        this.setState(__assign(__assign({}, this.state), { currency: currency }));
    };
    Object.defineProperty(Modal.prototype, "selectedPeriod", {
        get: function () {
            return this.state.period || this.firstPeriod;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "selectedCurrencyPeriod", {
        get: function () {
            return this.state.currency || this.getPayCurrency(this.selectedPeriod);
        },
        enumerable: false,
        configurable: true
    });
    Modal.prototype.checkPromo = function () {
        var _this = this;
        this.setState(__assign(__assign({}, this.state), { loadingPromo: true }));
        request_helper_1.RequestHelper.work(index_1.Pine.httpClient.post(config_1.CONFIG.checkPromoLink, { accountData: index_1.Pine.accountData, promo: this.state.promoCode }), function (data) {
            if (data === null || data === void 0 ? void 0 : data.status) {
                if (data.data.length) {
                    _this.setState(__assign(__assign({}, _this.state), { loadingPromo: false, blockedPromo: true }));
                    _this.setDiscount(data.data.find(function (item) { return item.owned; }) || data.data[0]);
                }
                else {
                    _this.setState(__assign(__assign({}, _this.state), { promoCode: null, loadingPromo: false, invalidPromo: true }));
                    setTimeout(function () { return _this.setState(__assign(__assign({}, _this.state), { invalidPromo: false })); }, 5000);
                }
            }
        });
    };
    Modal.prototype.send = function () {
        var _this = this;
        this.setState(__assign(__assign({}, this.state), { loading: true }));
        request_helper_1.RequestHelper.work(index_1.Pine.httpClient.post(config_1.CONFIG.payLink, { accountData: index_1.Pine.accountData, payData: this.state }), function (data) {
            if (data === null || data === void 0 ? void 0 : data.status) {
                _this.setState(__assign(__assign({}, _this.state), { loading: false, finished: true }));
                _this.close();
                if (config_1.CONFIG.payInModal) {
                    _this.payIframe(data.data.url);
                }
                else {
                    window.open(data.data.url, '_blank');
                }
            }
        });
    };
    Modal.prototype.payIframe = function (link) {
        var _this = this;
        var payBox = document.createElement('div');
        payBox.id = config_1.CONFIG.payIframeId;
        document.body.append(payBox);
        react_dom_1.default.render(react_2.default.createElement(pay_modal_1.PayModal, { link: link, onClose: function () { return _this.closePayIframe(); } }), payBox);
    };
    Modal.prototype.closePayIframe = function () {
        var payBox = document.getElementById(config_1.CONFIG.payIframeId);
        if (payBox) {
            payBox.remove();
        }
    };
    Modal.prototype.getUser = function () {
        return config_1.$APP.constant('user');
    };
    Modal.prototype.managers = function (isActive) {
        if (isActive === void 0) { isActive = true; }
        var result = [];
        var managers = config_1.$APP.constant('managers');
        for (var id in managers) {
            if (!managers.hasOwnProperty(id)) {
                continue;
            }
            result.push(managers[id]);
        }
        if (isActive) {
            return result.filter(function (item) { return item.active; });
        }
        return result;
    };
    Object.defineProperty(Modal.prototype, "selectPeriods", {
        get: function () {
            var _this = this;
            var selectItems = this.availablePeriods.map(function (item, index) {
                return react_2.default.createElement("li", { "data-value": index, className: "control--select--list--item ".concat(index === 0 ? 'control--select--list--item-selected' : ''), onClick: function () { return _this.setPeriod(item); }, key: index },
                    react_2.default.createElement("span", { className: "control--select--list--item-inner", title: "".concat(_this.getNumberPeriod(item), " ").concat(_this.getEnumPeriod(item)) },
                        _this.getNumberPeriod(item),
                        " ",
                        _this.getEnumPeriod(item)));
            });
            return react_2.default.createElement("div", { className: "control--select select-pay-period" },
                react_2.default.createElement("ul", { className: "custom-scroll control--select--list" }, selectItems),
                react_2.default.createElement("button", { className: "control--select--button", type: "button", "data-value": "0" },
                    react_2.default.createElement("span", { className: "control--select--button-inner" },
                        this.getNumberPeriod(this.selectedPeriod),
                        " ",
                        this.getEnumPeriod(this.selectedPeriod))),
                react_2.default.createElement("input", { type: "hidden", className: "control--select--input", name: "payPeriod", value: "0", "data-prev-value": "0", "data-change-on-init": "true" }));
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Modal.prototype, "selectCurrenciesPeriod", {
        get: function () {
            var _this = this;
            var selectedCP = this.selectedCurrencyPeriod;
            var selectItems = this.selectedPeriod.currency.map(function (item, index) {
                return react_2.default.createElement("li", { "data-value": index, className: "control--select--list--item ".concat(item === selectedCP ? 'control--select--list--item-selected' : ''), onClick: function () { return _this.setCurrencyPeriod(item); }, key: index },
                    react_2.default.createElement("span", { className: "control--select--list--item-inner", title: item.currency }, item.currency));
            });
            var selectedIndex = this.selectedPeriod.currency
                .reduce(function (result, item, index) { return item === selectedCP ? index : result; }, 0);
            return react_2.default.createElement("div", { className: "control--select select-pay-currency-period" },
                react_2.default.createElement("ul", { className: "custom-scroll control--select--list" }, selectItems),
                react_2.default.createElement("button", { className: "control--select--button", type: "button", "data-value": selectedIndex },
                    react_2.default.createElement("span", { className: "control--select--button-inner" }, selectedCP.currency)),
                react_2.default.createElement("input", { type: "hidden", className: "control--select--input", name: "payCurrencyPeriod", value: selectedIndex, "data-prev-value": "0", "data-change-on-init": "true" }));
        },
        enumerable: false,
        configurable: true
    });
    Modal.prototype.render = function () {
        var _this = this;
        var _a;
        var currencyPeriod = this.selectedCurrencyPeriod;
        var currencyPrice = react_2.default.createElement("span", { key: currencyPeriod.currency },
            react_2.default.createElement("span", null,
                this.totalPrice,
                " ",
                currencyPeriod.curVal));
        var currencyRedPrice = react_2.default.createElement("span", { key: currencyPeriod.currency },
            react_2.default.createElement("span", null,
                currencyPeriod.value,
                " ",
                currencyPeriod.curVal));
        return (react_2.default.createElement("div", { className: "details-modal" },
            react_2.default.createElement("div", { className: "details-modal-close", onClick: function () { return _this.close(); } },
                react_2.default.createElement("svg", { xmlns: "http://www.w3.org/2000/svg", width: "14", height: "14", viewBox: "0 0 14 14", fill: "none" },
                    react_2.default.createElement("path", { fillRule: "evenodd", clipRule: "evenodd", d: "M13.7071 1.70711C14.0976 1.31658 14.0976 0.683417 13.7071 0.292893C13.3166 -0.0976311 12.6834 -0.0976311 12.2929 0.292893L7 5.58579L1.70711 0.292893C1.31658 -0.0976311 0.683417 -0.0976311 0.292893 0.292893C-0.0976311 0.683417 -0.0976311 1.31658 0.292893 1.70711L5.58579 7L0.292893 12.2929C-0.0976311 12.6834 -0.0976311 13.3166 0.292893 13.7071C0.683417 14.0976 1.31658 14.0976 1.70711 13.7071L7 8.41421L12.2929 13.7071C12.6834 14.0976 13.3166 14.0976 13.7071 13.7071C14.0976 13.3166 14.0976 12.6834 13.7071 12.2929L8.41421 7L13.7071 1.70711Z", fill: "black" }))),
            react_2.default.createElement("div", { className: "details-modal-title" },
                react_2.default.createElement("h1", null, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C \u0432\u0438\u0434\u0436\u0435\u0442 \u043E\u043D\u043B\u0430\u0439\u043D")),
            react_2.default.createElement("div", { className: "details-modal-content" },
                react_2.default.createElement("div", { className: "main-content" },
                    react_2.default.createElement("div", { className: "line-select-data" },
                        react_2.default.createElement("div", null, "\u0412\u0430\u043B\u044E\u0442\u0430 \u0434\u043B\u044F \u043E\u043F\u043B\u0430\u0442\u044B:"),
                        react_2.default.createElement("div", { className: "right-line" }, this.isSingleCurrencyPeriod(this.selectedPeriod)
                            ? react_2.default.createElement("span", null, this.selectedCurrencyPeriod.currency)
                            : this.selectCurrenciesPeriod)),
                    react_2.default.createElement("div", { className: "line-select-data" },
                        react_2.default.createElement("div", null, "\u041F\u0435\u0440\u0438\u043E\u0434:"),
                        react_2.default.createElement("div", { className: "right-line" }, this.isSinglePeriod
                            ? react_2.default.createElement("span", null,
                                this.getNumberPeriod(this.selectedPeriod),
                                " ",
                                this.getEnumPeriod(this.selectedPeriod))
                            : this.selectPeriods)),
                    react_2.default.createElement("div", { className: "prices-data ".concat(this.isNoPromo ? 'with-discount' : '') },
                        this.isNoPromo && react_2.default.createElement("p", { className: "text-discount" }, this.state.discount.title),
                        this.isNoPromo && ((_a = this.state.discount.description) === null || _a === void 0 ? void 0 : _a.length) &&
                            react_2.default.createElement("p", { className: "desc-discount" }, this.state.discount.description),
                        react_2.default.createElement("div", { className: "text-price" }, currencyPrice),
                        this.isDiscount && react_2.default.createElement("div", { className: "text-price-red" }, currencyRedPrice),
                        this.selectedPeriod.users && react_2.default.createElement("p", { className: "text-users" },
                            react_2.default.createElement("span", null,
                                "\u041F\u043E \u0442\u0430\u0440\u0438\u0444\u0443: \u00AB\u0414\u043E ",
                                this.selectedPeriod.users,
                                " \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u0435\u0439\u00BB",
                                react_2.default.createElement("sup", null, "*")),
                            react_2.default.createElement("div", { className: "users-notice-info" },
                                react_2.default.createElement("sup", null, "*"),
                                "\u0422\u0430\u0440\u0438\u0444 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u044F\u0435\u0442\u0441\u044F \u0430\u0432\u0442\u043E\u043C\u0430\u0442\u0438\u0447\u0435\u0441\u043A\u0438, \u0438\u0441\u0445\u043E\u0434\u044F \u0438\u0437 \u043A\u043E\u043B\u0438\u0447\u0435\u0441\u0442\u0432\u0430 \u043F\u043E\u043B\u044C\u0437\u043E\u0432\u0430\u0442\u0435\u043B\u0435\u0439 \u0432 \u0430\u043A\u043A\u0430\u0443\u043D\u0442\u0435 Kommo (amoCRM)."))),
                    react_2.default.createElement("div", { className: "pine-promo-box" },
                        !this.state.blockedPromo &&
                            react_2.default.createElement("div", { className: "pine-promo-act" },
                                react_2.default.createElement("span", { onClick: function () { return _this.changePromoVisible(); } }, "\u0423 \u043C\u0435\u043D\u044F \u0435\u0441\u0442\u044C \u043F\u0440\u043E\u043C\u043E\u043A\u043E\u0434")),
                        this.state.showPromo &&
                            react_2.default.createElement("div", { className: "pine-promo-code" },
                                react_2.default.createElement("input", { name: "pine-promo-code", value: this.state.promoCode, onChange: function (e) { return _this.setFormState('promoCode', e); }, className: "text-input", type: "text", placeholder: "\u0412\u0430\u0448 \u043F\u0440\u043E\u043C\u043E\u043A\u043E\u0434", disabled: this.state.blockedPromo }),
                                react_2.default.createElement("button", { type: "button", className: "to-promo-btn button-input ".concat(this.disablePromoBtn ? 'button-input-disabled' : ''), onClick: function () { return _this.disablePromoBtn ? null : _this.checkPromo(); } },
                                    react_2.default.createElement("span", { className: "button-input-inner" },
                                        this.state.loadingPromo && react_2.default.createElement("span", { className: "spinner-icon" }),
                                        react_2.default.createElement("span", { className: "button-input-inner__text" }, "\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C")))),
                        react_2.default.createElement("div", { className: "info-promo" },
                            this.state.blockedPromo &&
                                react_2.default.createElement("span", { className: "success-promo" }, "\u041F\u0440\u043E\u043C\u043E\u043A\u043E\u0434 \u0443\u0441\u043F\u0435\u0448\u043D\u043E \u043F\u0440\u0438\u043C\u0435\u043D\u0435\u043D!"),
                            this.state.invalidPromo &&
                                react_2.default.createElement("span", { className: "error-promo" }, "\u041F\u0440\u043E\u043C\u043E\u043A\u043E\u0434 \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D \u0438\u043B\u0438 \u0441\u0440\u043E\u043A \u0434\u0435\u0439\u0441\u0442\u0432\u0438\u044F \u0438\u0441\u0442\u0435\u043A"))),
                    react_2.default.createElement("button", { type: "button", className: "to-pay-btn button-input button-input_blue ".concat(this.disableBtn ? 'button-input-disabled' : ''), onClick: function () { return _this.disableBtn ? null : _this.send(); } },
                        react_2.default.createElement("span", { className: "button-input-inner" },
                            this.state.loading && react_2.default.createElement("span", { className: "spinner-icon" }),
                            react_2.default.createElement("span", { className: "button-input-inner__text" }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C"))),
                    react_2.default.createElement("div", { className: "agg-privacy" },
                        react_2.default.createElement("span", null,
                            "\u0415\u0441\u043B\u0438 \u0432\u044B \u0445\u043E\u0442\u0438\u0442\u0435 \u043F\u0440\u043E\u0438\u0437\u0432\u0435\u0441\u0442\u0438 \u043E\u043F\u043B\u0430\u0442\u0443 \u0434\u0440\u0443\u0433\u0438\u043C \u0441\u043F\u043E\u0441\u043E\u0431\u043E\u043C, ",
                            react_2.default.createElement("a", { href: config_1.CONFIG.contactPage, target: "_blank" }, "\u0441\u0432\u044F\u0436\u0438\u0442\u0435\u0441\u044C \u0441 \u043D\u0430\u043C\u0438"),
                            "."))),
                react_2.default.createElement("div", { className: "footer-content" },
                    react_2.default.createElement("div", { className: "flex-contacts" },
                        react_2.default.createElement("div", { className: "contacts-phones" },
                            react_2.default.createElement("p", null,
                                react_2.default.createElement("a", { href: 'tel:' + config_1.CONFIG.phone1.replace(/[^+\d]/, '') }, config_1.CONFIG.phone1)),
                            react_2.default.createElement("p", null,
                                react_2.default.createElement("a", { href: 'tel:' + config_1.CONFIG.phone2.replace(/[^+\d]/, '') }, config_1.CONFIG.phone2))),
                        react_2.default.createElement("div", { className: "contacts-site" },
                            react_2.default.createElement("a", { href: config_1.CONFIG.site, target: "_blank" }, "fabrika-klientov.ua")))))));
    };
    return Modal;
}(react_1.Component));
exports.Modal = Modal;
//# sourceMappingURL=pine-modal.js.map