"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Overload = void 0;
var react_1 = __importStar(require("react"));
var pine_modal_1 = require("./pine-modal");
var Overload = (function (_super) {
    __extends(Overload, _super);
    function Overload() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Overload.prototype.onCLose = function () {
        this.detailsRef.removeAttribute('open');
    };
    Overload.prototype.setRef = function (el) {
        this.detailsRef = el;
    };
    Overload.prototype.render = function () {
        var _this = this;
        return (react_1.default.createElement("details", { ref: function (el) { return _this.setRef(el); } },
            react_1.default.createElement("summary", null,
                react_1.default.createElement("div", { className: "button-input button-input_blue" },
                    react_1.default.createElement("span", { className: "button-input-inner" },
                        react_1.default.createElement("span", { className: "button-input-inner__text" }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C \u0432\u0438\u0434\u0436\u0435\u0442"))),
                react_1.default.createElement("div", { className: "details-modal-overlay" })),
            react_1.default.createElement(pine_modal_1.Modal, { onClose: function () { return _this.onCLose(); }, pricePeriods: this.props.pricePeriods, discountList: this.props.discountList })));
    };
    return Overload;
}(react_1.Component));
exports.Overload = Overload;
//# sourceMappingURL=pine-overload.js.map