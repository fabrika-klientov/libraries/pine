export declare const CONFIG: {
    payLink: string;
    checkPromoLink: string;
    server: string;
    serverText: string;
    statusLink: string;
    statusText: string;
    privacy1: string;
    privacy2: string;
    contactPage: string;
    phone1: string;
    phone2: string;
    site: string;
    payInModal: boolean;
    payIframeId: string;
};
export declare const $APP: any;
