"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PayModal = void 0;
var react_1 = require("react");
var react_2 = __importDefault(require("react"));
var context = null;
var clickBodyListener = function (event) {
    var target = event.target;
    if (context) {
        if (target === context.modalRef || context.modalRef.contains(target)) {
            return;
        }
        context.close();
    }
};
var PayModal = (function (_super) {
    __extends(PayModal, _super);
    function PayModal(props) {
        var _this = _super.call(this, props) || this;
        context = _this;
        _this.state = {};
        return _this;
    }
    PayModal.prototype.componentDidMount = function () {
        this.removeListeners();
        this.addListeners();
    };
    PayModal.prototype.componentWillUnmount = function () {
        this.removeListeners();
    };
    PayModal.prototype.addListeners = function () {
        document.body.addEventListener('click', clickBodyListener, false);
    };
    PayModal.prototype.removeListeners = function () {
        document.body.removeEventListener('click', clickBodyListener, false);
    };
    PayModal.prototype.setRef = function (el) {
        this.modalRef = el;
    };
    PayModal.prototype.close = function () {
        this.props.onClose();
    };
    PayModal.prototype.render = function () {
        var _this = this;
        return (react_2.default.createElement("div", { className: "p-p-modal-overload" },
            react_2.default.createElement("div", { ref: function (el) { return _this.setRef(el); }, className: "p-p-modal" },
                react_2.default.createElement("iframe", { src: this.props.link, frameBorder: "0" }))));
    };
    return PayModal;
}(react_1.Component));
exports.PayModal = PayModal;
//# sourceMappingURL=pay-modal.js.map