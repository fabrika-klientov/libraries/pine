import { Component } from 'react';
interface ModalProps {
    link: string;
    onClose(): void;
}
export declare class PayModal extends Component<ModalProps, any> {
    modalRef: HTMLElement;
    constructor(props: any);
    componentDidMount(): void;
    componentWillUnmount(): void;
    protected addListeners(): void;
    protected removeListeners(): void;
    setRef(el: HTMLElement | any): void;
    close(): void;
    render(): JSX.Element;
}
export {};
