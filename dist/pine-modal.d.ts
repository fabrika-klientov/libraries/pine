import { ChangeEvent, Component } from 'react';
import { PricePeriodCurrency, PricePeriodSettings } from './price-period.model';
import { AmoManager, AmoUser } from './amo-user.model';
import { DiscountModel } from './discount.model';
interface ModalProps {
    pricePeriods: PricePeriodSettings[];
    discountList: DiscountModel[];
    onClose(): void;
}
interface ModalState {
    name: string;
    phone: string;
    email: string;
    period: PricePeriodSettings | null;
    currency: PricePeriodCurrency | null;
    discount: DiscountModel | null;
    promoCode: string | null;
    loading?: boolean;
    loadingPromo?: boolean;
    showPromo: boolean;
    blockedPromo?: boolean;
    invalidPromo?: boolean;
    finished?: boolean;
}
export declare class Modal extends Component<ModalProps, ModalState> {
    private currenciesOrderList;
    constructor(props: any);
    componentDidMount(): void;
    get availablePeriods(): PricePeriodSettings[];
    get disableBtn(): boolean;
    get disablePromoBtn(): boolean;
    get isSinglePeriod(): boolean;
    isSingleCurrencyPeriod(period: PricePeriodSettings): boolean;
    get firstPeriod(): PricePeriodSettings;
    getPayCurrency(model: PricePeriodSettings): PricePeriodCurrency;
    getNumberPeriod(model: PricePeriodSettings): number;
    getEnumPeriod(model: PricePeriodSettings): string;
    get isDiscount(): boolean;
    get isPromo(): boolean;
    get isNoPromo(): boolean;
    get totalPrice(): number | string;
    close(): void;
    setFormState(attr: string, ev: ChangeEvent<HTMLInputElement>): void;
    setDiscount(discount: DiscountModel): void;
    changePromoVisible(): void;
    setPeriod(period: PricePeriodSettings): void;
    setCurrencyPeriod(currency: PricePeriodCurrency): void;
    get selectedPeriod(): PricePeriodSettings;
    get selectedCurrencyPeriod(): PricePeriodCurrency;
    checkPromo(): void;
    send(): void;
    protected payIframe(link: string): void;
    protected closePayIframe(): void;
    protected getUser(): AmoUser;
    managers(isActive?: boolean): AmoManager[];
    get selectPeriods(): JSX.Element;
    get selectCurrenciesPeriod(): JSX.Element;
    render(): JSX.Element;
}
export {};
