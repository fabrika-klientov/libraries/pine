"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pine = void 0;
require("./styles.scss");
var react_dom_1 = __importDefault(require("react-dom"));
var react_1 = __importDefault(require("react"));
var pine_overload_1 = require("./pine-overload");
var pine_bar_1 = require("./pine-bar");
var config_1 = require("./config");
var request_helper_1 = require("./request.helper");
var Pine = (function () {
    function Pine(element, httpClient) {
        this.element = element;
        if (typeof this.element === 'string') {
            this.element = document.querySelector(this.element);
        }
        Pine.http = httpClient;
    }
    Pine.prototype.init = function (account) {
        var _a;
        Pine.accountData = account;
        if (!this.element) {
            return;
        }
        if (((_a = account.widgetParams) === null || _a === void 0 ? void 0 : _a.active) !== 'Y') {
            return;
        }
        react_dom_1.default.render(react_1.default.createElement(pine_overload_1.Overload, { pricePeriods: account.pricePeriods, discountList: account.discountList || [] }), this.element);
    };
    Pine.prototype.initBar = function (account) {
        var _this = this;
        if (!this.element) {
            return;
        }
        request_helper_1.RequestHelper.work(Pine.http.get(config_1.CONFIG.server + config_1.CONFIG.statusLink + account.key), function (result) {
            request_helper_1.RequestHelper.work(Pine.http.get(config_1.CONFIG.serverText + (config_1.CONFIG.statusText
                .replace('{code}', result.code)
                .replace('{lang}', 'ru'))), function (status) {
                var description = status || '';
                for (var key in result.additional) {
                    description = description.replace(key, result.additional[key]);
                }
                var alert = {
                    type: result.type,
                    title: ['success', 'info'].includes(result.type)
                        ? 'Виджет активен'
                        : (result.type === 'warning' ? 'Предупреждение' : 'Внимание'),
                    description: description,
                };
                react_dom_1.default.render(react_1.default.createElement(pine_bar_1.PineBar, { alert: alert }), _this.element);
            });
        });
    };
    Pine.prototype.withStyles = function () {
        var parentBox = this.element.classList.contains('widget_settings_block__controls')
            ? this.element : this.element.closest('.widget_settings_block__controls');
        if (parentBox) {
            parentBox.classList.add('flex-pay-btn-box');
        }
        return this;
    };
    Object.defineProperty(Pine, "httpClient", {
        get: function () {
            return Pine.http;
        },
        enumerable: false,
        configurable: true
    });
    return Pine;
}());
exports.Pine = Pine;
//# sourceMappingURL=index.js.map